FROM openjdk:17-alpine
EXPOSE 8080
ADD target/helloworld.jar helloworld
ENTRYPOINT ["java", "-jar","helloworld"]
